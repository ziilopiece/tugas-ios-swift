import UIKit

//1. Tugas variable and constant 1

let y = 30
var x = y + 10
print(x) // Output: 40

//2. Tugas variable and constant 2

let greeting = "Selamat datang di"
let place = "Kopi Surgawi"

let content = greeting + " " + place
print(content) // Output: Selamat datang di Kopi Surgawi

let count = content.count
print(count) // Output: 26

let uppercaseContent = content.uppercased()
print(uppercaseContent) // Output: SELAMAT DATANG DI KOPI SURGAWI

let lowercaseContent = content.lowercased()
print(lowercaseContent) // Output: selamat datang di kopi surgawi


//3. Tugas variable and constant 3

let coffeePrice = 20000
let amountOfCoffee = 25
let totalPrice = coffeePrice * amountOfCoffee

let billMessage = "Total price you should pay is IDR \(totalPrice)"

print(billMessage)

//LATIHAN IF STATMENTS

//1. Latihan 1

let wakeUpTime = 5

if wakeUpTime >= 6 {
    print("You will be late, Rana!")
} else {
    print("Way to go! Keep Spirit")
}

//2. Latihan 2

let pouringAmount = 120 // contoh nilai pouring kopi

switch pouringAmount {
case 30..<90:
    print("Second interval, after then wait until thirty second")
case 90..<180:
    print("Third interval, after then wait until twenty five second")
case 180..<225:
    print("Resting interval and your coffee are ready to serve")
case 225...:
    print("Over resting")
default:
    print("First interval and start pouring")
}

//LATIHAN FOR IN LOOPS

//1. Latihan 1

let customers = ["Anna", "Hendry", "Disa", "Wendy", "Ryan"]

for customer in customers {
    print("Selamat datang di Kopi Surgawi, \(customer)!")
}

//2. Latihan 2

let menuKopi = [
    "Anna": "Vanilla Latte",
    "Hendry": "Cold White",
    "Disa": "Ice Americano",
    "Wendy": "Ice Japanese",
    "Ryan": "Ice Coffee Surgawi"
]

for (nama, menu) in menuKopi {
    print("Pesanan \(menu) atas nama kak \(nama) sudah ready!")
}

